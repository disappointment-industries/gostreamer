module gitlab.com/disappointment-industries/gostreamer

go 1.16

require (
	gitlab.com/MikeTTh/env v0.0.0-20210102155928-2e9be3823cc7 // indirect
	gopkg.in/yaml.v2 v2.4.0
)
