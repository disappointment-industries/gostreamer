package main

import (
	"fmt"
	"gitlab.com/MikeTTh/env"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"sync"
)

func main() {
	inputfile := os.Args[1]

	email := os.Args[2]

	videos := process(inputfile)

	first(email)

	var wg sync.WaitGroup

	// loop on videos to the same output directory
	done := 0
	all := 0

	for _, outDir := range videos {
		// create output directory if needed
		e := os.MkdirAll(outDir.Path, os.ModePerm)
		if e != nil {
			fmt.Println(e)
		}

		for _, v := range outDir.Lista {
			cmd := exec.Command("./destreamer.sh", "-i", v.Url, "-o", outDir.Path,
				"-t", v.Name,
				"--skip", "-x") // --skip for prevent multiple downloads, -x turn off thumbnails in terminal
			cmd.Dir = "destreamer"
			wg.Add(1)
			all++
			go func(v video, p string) {
				fmt.Println("starting download:", v.Name, "=>", p)
				b, er := cmd.CombinedOutput()
				if er != nil {
					fmt.Println(string(b))
					fmt.Println(er.Error())
					fmt.Println(v.Name, "failed")
				}
				done++
				fmt.Println("done,", v.Name, "downloaded to", p) // TODO: jobb visszajelzes
				fmt.Printf("so far: %d/%d (%f%%)\n", done, all, float64(done)/float64(all)*100.0)
				wg.Done()
			}(v, outDir.Path)
		}
	}

	wg.Wait()
}

func first(email string) {

	url := env.GetOrDosomething("URL", func() string {
		panic("i have no Url to log you in")
	})

	cmd := exec.Command("./destreamer.sh",
		"-s", "-u", email,
		"-i", url) // email dont need check

	cmd.Dir = "destreamer"

	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	err := cmd.Run()
	if err != nil {
		log.Printf("Command finished with error: %v", err)
	}
}

type video struct {
	Url  string `yaml:"url"`
	Name string `yaml:"name"`
}

type videoList struct {
	Path  string  `yaml:"path"`
	Lista []video `yaml:"lista"`
}

func process(filename string) []videoList {
	b, e := ioutil.ReadFile(filename)
	if e != nil {
		panic(e)
	}

	var videos []videoList

	e = yaml.Unmarshal(b, &videos)
	if e != nil {
		fmt.Println(e)
	}

	return videos
}
